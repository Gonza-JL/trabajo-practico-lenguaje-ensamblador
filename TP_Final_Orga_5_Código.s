.data
indicacionMSJ: .ascii "Ingrese el input en minusculas: "
longitudIndicacionMSJ = . - indicacionMSJ

mensaje: .asciz "                                                                                                         "
longitudMensaje = . - mensaje

charsProcesadosMSJ: .ascii "Caracteres procesados:   "
longitudCharsProcesadosMSJ = . - charsProcesadosMSJ

decodificadoMSJ: .ascii "Mensaje: "
longitudDecodificadoMSJ = . - decodificadoMSJ

desplazamientoMSJ: .ascii "Desplazamiento usado: "
longitudDesplazamientoMSJ = . - desplazamientoMSJ

enter: .ascii "\n"

numero: .word 00

claveAsciz: .asciz "    "
longitudClaveAsciz = . - claveAsciz

inputUsuario:  .asciz "                                                                                                   "
longitudInputUsuario = . - inputUsuario
.text
@----------------------------------------------------------
@Parametros inputs: no tiene
@Parametros output:
@r1 = dirección de memoria del inputUsuario
leerInput:
.fnstart
    push {r0, r2, r7}
    mov r7, #3  @ Lectura por teclado
    mov r0, #0  @ Ingreso de cadena
    ldr r2, =longitudInputUsuario  @ Lee # caracteres
    ldr r1, =inputUsuario  @ Donde se guarda la cadena ingresada
    swi 0  @ SWI, Software interrupt
    pop {r0, r2, r7}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r1 = dirección de memoria del inputUsuario
@Parametros output:
@r0 = longitud del mensaje(sin la clave ni la opción)
extraerMensaje:
.fnstart
    push {r2, r3}
    ldrb r0, [r1]     @ Guarda el primer caracter en r0
    ldr r3, =mensaje  @ Apunta a la dirección del mensaje
    mov r2, #0
ciclo:
    cmp r0, #';'  @ Compara un carácter con ';'
    beq fin       @ Si r0 == ';' salta a fin
    strb r0, [r3, r2]  @ Guarda en mensaje un carácter
    add r2, #1         @ Cuenta los caracteres del mensaje
    ldrb r0, [r1, r2]  @ Guarda un carácter en r0
    b ciclo  @ Vuelve a ciclo
fin:
    mov r0, r2  @ Copia la longitud del mensaje a r0
    pop {r2, r3}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r0 = longitud del mensaje(sin la clave ni la opción)
@r1 = dirección de memoria del inputUsuario
@Parametros output: no tiene
extraerClave:
.fnstart
    push {r0, r1, r2}
    add r0, #1             @ longitudMensaje + 1 (para saltar el ';' entre el mensaje y la clave)
    add r1, r0             @ Le suma a r1 la longitudMensaje+1
    ldrb r2, [r1]          @ Guarda el primer byte de la clave en r2
    ldr r0, =claveAsciz    @ Apunta a claveAsciz
ciclo.:
    cmp r2, #';' @ Compara un byte con ';'
    beq end      @ Si r2 = ';' salta a end
    strb r2, [r0]  @ Guarda un byte en claveAsciz
    add r0, #1     @ Se mueve un lugar en claveAsciz para guardar el próximo byte
    add r1, #1     @ Se mueve un lugar en InputUsuario
    ldrb r2, [r1]  @ Guarda un byte en r0
    b ciclo.  @ Salta a ciclo.
end:
    pop {r0, r1, r2}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r0 = longitud del mensaje(sin la clave ni la opción)
@r1 = dirección de memoria del inputUsuario
@Parametros output:
@r1 = opción ('c' o 'd')
extraerOpcion:
.fnstart
    push {r0}
    add r0, #7
    add r1, r0  @ r1 apunta al último byte de inputUsuario
buscaOpcion:
    cmp r0, #'c'
    beq fin.   @ Si r0 = 'c' salta a fin
    cmp r0, #'d'
    beq fin.   @ Si r0 = 'd' salta a fin
    sub r1, #1  @ Le resta un byte a la dirección que apunta r1
    ldrb r0, [r1]  @ Guarda un byte en r0
    b buscaOpcion
fin.:
    mov r1, r0  @ Copia la opción a r0
    pop {r0}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r0 = clave
@r1 = dirección de memoria del mensaje
@Parametros output: no tiene
codificar:
.fnstart
    push {r1, r2}
    ldrb r2, [r1]  @ Guarda el primer caracter en r2
cambiarLetras:
    cmp r2, #0
    beq terminar  @ Si r2 = 0 salta a terminar (si llego al final del mensaje)
    cmp r2, #' '
    beq iterar  @ Si r2 = espacio salta a iterar
    add r2, r0  @ Desplaza el carácter r0 veces en la tabla ascii
    cmp r2, #122
    bls guardaCaracter  @ Si r2 <= 122(posicion de la última letra de la tabla ascii) salta a guardaCaracter
resta:
    cmp r2, #122
    bls guardaCaracter
    sub r2, #26  @ Resta 26 posiciones de la tabla ascii a r2
    b resta  @ Vuelve a resta
guardaCaracter:
    strb r2, [r1]  @ Reemplaza el carácter en mensaje
iterar:
    add r1, #1  @ Pasa al siguiente carácter en mensaje
    ldrb r2, [r1]  @ Guarda en r2 el carácter
    b cambiarLetras  @ Vuelve a cambiarLetras
terminar:
    pop {r1, r2}
    bx lr  @ Vuelve a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r1 = palabra clave
@Parametros output:
@r0 = clave que se usó para decodificar
decodificar:
.fnstart
    push {r2, r3, r4}
    mov r4, #0
codifica1:
    ldr r2, =mensaje  @ Apunta a la dirección del mensaje
    mov r0, #1
    add r4, #1  @ Cuenta cuantas veces se codificó(para sacar la clave)
    push {r1, lr}  @ Guarda el valor de r1 y lr
    mov r1, r2  @ Copia la dirección del mensaje en r1
    bl codificar  @ Codifica una posición(r0 = 1) a mensaje
    pop {r1, lr}  @ Le devuelve su valor a r1 y a lr
    ldr r3, [r2]  @ Guarda 4 caracteres del mensaje en r3
compara:
    cmp r3, r1
    beq finaliza   @ Si r3 == palabra clave(r3 = 4 caracteres del mensaje) salta a finaliza
    ldrb r0, [r2]  @ Guarda un byte del mensaje en r0
    cmp r0, #0
    beq codifica1  @ Si r0 == 0, es decir que recorrimos todo el mensaje, salta a codifica1
    add r2, #1     @ Se mueve un byte en la dirección del mensaje
    ldr r3, [r2]   @ Guarda 4 caracteres del mensaje en r3
    b compara  @ Vuelve a compara
finaliza:
    mov r0, r4  @ Copia en r0 la clave que se usó para decodificar
    pop {r2, r3, r4}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r1 = el dividendo
@r2 = el divisor
@Parametros output:
@r0 = el resultado de la división
division:
.fnstart
    @r1 = es el resto de la división
    mov r0, #0  @ En r0 vamos a contar cuántas veces entra B en A, es decir, r0 contendrá la división A/B
while:
    cmp r1, r2  @ Condicional del ciclo
    bcc  finCiclo  @ Salgo del ciclo si no se cumple r1 >= r2, es decir si r1 < r2
    sub r1, r2  @ r1 = r1 - r2
    add r0, #1  @ r0 = r0 + 1
    b while
finCiclo:
    bx lr @ Salimos de la función
.fnend
@----------------------------------------------------------
@Solo para números de uno o dos dígitos
@Parametros inputs:
@r0=el entero a convertir
@Parametros output:
@r1= el código ascii de las unidades
@r2= el código ascii de las decenas
convertirEnteroAAscii:
.fnstart
    push {lr}
    @ Calcular los pesos (r0)
    @ Calculamos las decenas en el resto que me quedo de la anterior división
    @ r2 = las decenas: 2  (000...00010)
    @ r2 = r0/10
    mov r1, r0    @ r1 = A
    mov r2, #10   @ r2 = B
    bl division   @ r0 = A/B, r1 = resto(A/B)
    mov r2, r0    @ r2 = r0/10  @ En r2 tenemos las decenas
    mov r0, r1  @ r0 = resto(r0/10)

    @ Calculamos las unidades
    @ r1 = la unidades: 2
    @ r1 = r0
    mov r1, r0
    add r1, #48  @ Código ascii de las unidades
    add r2,#48  @ Código ascii de las decenas
    pop {lr}
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Solo para numeros de uno o dos dígitos
@Parametros inputs:
@r0 = dirección de memoria del ascii
@Parametros output:
@r0 = entero
convertirAsciiAEntero:
.fnstart
    ldrb r1, [r0]  @ Guarda en r1 el primer byte del ascii
    mov r2, #0
    mov r3, #10
    cmp r1, #'-'
    bne convertir  @ Si r1 != '-' salta a convertir
    ldr r4, [r0]  @ Guarda en r4 el '-'
    add r0, #1    @ Pasa al siguiente byte
    ldrb r1, [r0] @ Guarda el byte en r1
convertir:
    sub r1, #48   @ Convierte un byte del ascii a entero
    mov r2, r1  @ Guarda el primer número en r2
    add r0, #1  @ Pasa al siguiente byte
    ldrb r1, [r0]  @ Guarda un byte en r1
    cmp r1, #' '
    beq guardaNum  @ Si r1 == ' ' es un número de un dígito y salta a guardaNum
    mul r2, r3  @ Multiplica el número por 10 y guarda el resultado en r2
    sub r1, #48  @ Convierte un byte del ascii a entero
    add r2, r1   @ Le suma a r2 el último número
guardaNum:
    mov r0, r2   @ Copia en r0 el número convertido
    @ Verifica si era un número negativo y si lo es lo pasa a complemento a2
    cmp r4, #'-'
    bne termina  @ Si no es negativo salta a termina
    eor r0, #0xff  @ Invierte los bits del número
    add r0, #1  @ Le suma 1 al número con los bits invertidos
termina:
    bx lr  @ Volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
@Parametros inputs:
@r1=puntero al string que queremos imprimir
@r2=longitud de lo que queremos imprimir
@Parametros output: no tiene
imprimirString:
.fnstart
    push {r0, r7}
    mov r7, #4 @ Salida por pantalla
    mov r0, #1      @ Indicamos a SWI que será una cadena
    swi 0    @ SWI, Software interrupt
    pop {r0, r7}
    bx lr @salimos de la función mifuncion
.fnend
@----------------------------------------------------------
newLine:
.fnstart
    push {lr}
    mov r2, #1 @Tamaño de la cadena
    ldr r1, =enter   @Cargamos en r1 la dirección del mensaje
    bl imprimirString
    pop {lr}
    bx lr @salimos de la función mifuncion
.fnend
@----------------------------------------------------------
.global main
main:
    @Imprime "Ingrese el input en minusculas: "
    ldr r2, =longitudIndicacionMSJ  @ Tamaño de la cadena
    ldr r1, =indicacionMSJ  @ Cargamos en r1 la direccion del mensaje
    bl imprimirString

    bl leerInput
    bl extraerMensaje
    bl extraerClave
    bl extraerOpcion

    cmp r1, #'c'  @ Compara la opción con 'c'
    bne esDecodificar  @ Si la opción != c salta a esDecodificar, en caso contrario va a codificar

    push {r0}  @ Hace push de los caracteres procesados

    @ Codifica
    ldr r0, =claveAsciz  @ Apunta a claveAsciz
    bl convertirAsciiAEntero  @ Convierte a entero al ascii que apunta r0
    ldr r1, =mensaje  @ Apunta al mensaje
    bl codificar  @ Codifica el mensaje que apunta r1

    @ Imprime el mensaje codificado
    ldr r2, =longitudMensaje  @ Tamaño de la cadena
    ldr r1, =mensaje          @ Cargamos en r1 la dirección del mensaje
    bl imprimirString
    bl newLine

    pop {r0}  @ Hace pop de los caracteres procesados
    bl convertirEnteroAAscii  @ Convierte a ascii a la cantidad de caracteres procesados
    ldr r0, =charsProcesadosMSJ  @ Apunta a "Caracteres procesados:   "
    strb r2, [r0, #23]  @ Guarda el primer digito del numero(chars procesados) casi al final de charsProcesadosMSJ
    strb r1, [r0, #24]  @ Guarda el segundo digito del numero(chars procesados) al final de charsProcesadosMSJ

    ldr r2, =longitudCharsProcesadosMSJ  @ Tamaño de la cadena
    ldr r1, =charsProcesadosMSJ  @ Cargamos en r1 la dirección de "Caracteres procesados: ..."
    bl imprimirString
    bl newLine
    b finalizar  @ Salta a finalizar

esDecodificar:
    cmp r1, #'d'
    bne finalizar  @ Si la opción != d va a finalizar, en caso contrario va a decodificar

    @ Decodifica
    ldr r1, =claveAsciz  @ Apunta a la palabra clave
    ldr r1, [r1]  @ Guarda en r1 la palabra clave(solo 4 caracteres)
    bl decodificar

    @ Imprime "Mensaje: " con el mensaje decodificado
    ldr r2, =longitudDecodificadoMSJ  @ Tamaño de la cadena
    ldr r1, =decodificadoMSJ  @ Cargamos en r1 la dirección de "Mensaje: "
    bl imprimirString
    ldr r2, =longitudMensaje  @ Tamaño de la cadena
    ldr r1, =mensaje          @ Cargamos en r1 la dirección del mensaje
    bl imprimirString
    bl newLine

    bl convertirEnteroAAscii
    ldr r0, =claveAsciz  @ r0 apunta a claveAsciz
    str r2, [r0]  @ Guarda en claveAsciz el primer dígito de la clave utilizada
    strb r1, [r0, #1]  @ Guarda el segundo dígito en claveAsciz al lado del primer dígito de la clave utilizada

    @ Imprime "Desplazamiento usado: " con la clave que se usó para decodificar
    ldr r2, =longitudDesplazamientoMSJ  @ Tamaño de la cadena
    ldr r1, =desplazamientoMSJ  @ Cargamos en r1 la dirección de "Desplazamiento Usado: "
    bl imprimirString
    ldr r2, =longitudClaveAsciz  @ Tamaño de la cadena
    ldr r1, =claveAsciz  @ Cargamos en r1 la dirección de la clave utilizada
    bl imprimirString
    bl newLine

finalizar:
    mov r7, #1    @ Salida al sistema
    swi 0
